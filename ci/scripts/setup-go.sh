#!/bin/bash

PROGNAME=pubsubbeat
GODIR=gitlab.com/gl-infra/pubsubbeat

mkdir -p $GOPATH/src/gitlab.com/gl-infra
ln -s $PWD/src $GOPATH/src/$GODIR

cd $GOPATH/src/$GODIR
echo "Gopath is: " $GOPATH
echo "pwd is: " $PWD
ls -lah

